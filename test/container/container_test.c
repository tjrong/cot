
#include <stdio.h>
#include "unity.h"

int test_List(void);
int test_Queue(void);
int test_IndQueue(void);
int test_Stack(void);
int test_Vector(void);

void setUp() {
    // 这里可以放置每个测试用例运行前的初始化代码
}

void tearDown() {
    // 这里可以放置每个测试用例运行后的清理代码
}

int main()
{
    test_List();
    test_Queue();
    test_IndQueue();
    test_Stack();
    test_Vector();

    return 0;
}