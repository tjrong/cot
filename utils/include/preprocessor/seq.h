# ifndef COT_PREPROCESSOR_SEQ_SEQ_H
# define COT_PREPROCESSOR_SEQ_SEQ_H

# include <preprocessor/elem.h>

# define COT_PP_SEQ_HEAD(seq) COT_PP_SEQ_ELEM(0, seq)


# define COT_PP_SEQ_TAIL(seq) COT_PP_SEQ_TAIL_D(seq)
# define COT_PP_SEQ_TAIL_D(seq) COT_PP_SEQ_TAIL_I seq

# define COT_PP_SEQ_TAIL_I(x)

# define COT_PP_SEQ_NIL(x) (x)

# endif

